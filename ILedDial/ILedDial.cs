﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace ILedDialNS
{
    public interface ILedDial
    {
        void StatusRequest();
        void SetLedRequest(int ledPosition, Color color);
        void ClearLedRequest(int ledPosition);
        void SetLedRangeRequest(int startLedPosition, int endLedPosition, Color color);
        void ClearAllRequest();
        void SetAllRequest(Color color);
        void GetVersionRequest();
        void SetFrameRequest(List<Color> ledColors);
    }
}
